﻿namespace ForeignExchange.ViewModels
{
    using ForeignExchange.Helpers;
    using ForeignExchange.Models;
    using ForeignExchange.Services;
    using GalaSoft.MvvmLight.Command;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Windows.Input;
    using Xamarin.Forms;

    public class MainViewModel : INotifyPropertyChanged
    {
        #region Events
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region Services
        ApiService apiService;
        DialogService dialogService;
        DataService dataservice;
        #endregion

        #region Attributes
        bool _IsRunning;
        bool _IsEnabled;
        string _Result;
        ObservableCollection<Rate> _Rates;
        Rate _SourceRate;
        Rate _TargetRate;
        string _Status;
        #endregion

        #region Properties
        public string Amount { get; set; }

        public string Status
        {
            get
            {
                return _Status;
            }

            set
            {
                if (_Status != value)
                {
                    _Status = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Status)));
                }
            }
        }

        public ObservableCollection<Rate> Rates
        {
            get
            {
                return _Rates;
            }

            set
            {
                if (_Rates != value)
                {
                    _Rates = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Rates)));
                }
            }
        }

        public Rate SourceRate
        {
            get
            {
                return _SourceRate;
            }

            set
            {
                if (_SourceRate != value)
                {
                    _SourceRate = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(SourceRate)));
                }
            }
        }
        public Rate TargetRate
        {
            get
            {
                return _TargetRate;
            }

            set
            {
                if (_TargetRate != value)
                {
                    _TargetRate = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(TargetRate)));
                }
            }
        }

        public bool IsRunning
        {
            get
            {
                return _IsRunning;
            }

            set
            {
                if (_IsRunning != value)
                {
                    _IsRunning = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(IsRunning)));
                }
            }
        }

        public bool IsEnabled
        {
            get
            {
                return _IsEnabled;
            }

            set
            {
                if (_IsEnabled != value)
                {
                    _IsEnabled = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(IsEnabled)));
                }
            }
        }

        public string Result
        {
            get
            {
                return _Result;
            }

            set
            {
                if (_Result != value)
                {
                    _Result = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Result)));
                }
            }
        }
        #endregion

        #region Constructors
        public MainViewModel()
        {
            apiService = new ApiService();
            dataservice = new DataService();
            dialogService = new DialogService();
            LoadRates();
        }


        #endregion

        #region Methods
        async void LoadRates()
        {
            IsRunning = true;
            Result = "Loading Rates...";

            var connection = await apiService.CheckConnection();

            if (!connection.IsSuccess)
            {
                IsRunning = true;
                Result = connection.Message;
                Status = "Rates Loaded From Database";
                return;
            }

            var URL = "https://apiexchangerates.azurewebsites.net";//Application.Current.Resources["URLAPI"].ToString();

            var response = await apiService.GetList<Rate>(URL, "/api/Rates");

            if (!response.IsSuccess)
            {
                //await Application.Current.MainPage.DisplayAlert(Lenguages.Error, response.Message, Lenguages.Accept);
                IsRunning = false;
                Result = response.Message;
                return;
            }


            var rates = (List<Rate>)response.Result;
            dataservice.DeleteAll<Rate>();
            dataservice.Save(rates);

            Rates = new ObservableCollection<Rate>(rates);
            IsRunning = false;
            IsEnabled = true;
            Result = "Ready to Convert";
            Status = "Rates Loaded From Internet";
           
        }
        #endregion

        #region Commands
        public ICommand ConvertCommand
        {
            get
            {
                return new RelayCommand(Convert);
            }
        }

        public ICommand SwitchCommand
        {
            get
            {
                return new RelayCommand(Switchs);
            }
        }

        void Switchs()
        {
            var aux = SourceRate;
            SourceRate = TargetRate;
            TargetRate = aux;

            Convert();
        }

        async void Convert()
        {
            // validamos que hayan seleccionado los datos necesari para realizar operacion
            if (string.IsNullOrEmpty(Amount))
            {
                //await Application.Current.MainPage.DisplayAlert(Lenguages.Error, Lenguages.AmountValidation, Lenguages.Accept);
                await dialogService.ShowMessage(Lenguages.Error, Lenguages.AmountValidation);
                return;
            }

            decimal amount = 0;

            if (!decimal.TryParse(Amount, out amount))
            {
                await dialogService.ShowMessage(Lenguages.Error, Lenguages.AmountNumericValidation);
                return;
            }            

            if (SourceRate == null)
            {
                await dialogService.ShowMessage(Lenguages.Error, "You Must Select a SourceRate.");
                return;
            }

            if (TargetRate == null)
            {
                await dialogService.ShowMessage(Lenguages.Error, "You Must Select a TargetRate.");                
                return;
            }

            var amountConverted = amount / (decimal)SourceRate.TaxRate * (decimal)TargetRate.TaxRate;


            Result = string.Format("{0} {1:C2} = {2} {3:C3}", SourceRate.Code, amount, TargetRate.Code, amountConverted);
        }
        #endregion
    }
}
