﻿using ForeignExchange.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ForeignExchange.Services
{
    public class DialogService
    {
        public async Task ShowMessage(string title, string messsage)
        {
            await Application.Current.MainPage.DisplayAlert(title, messsage, Lenguages.Accept);
        }
    }
}
